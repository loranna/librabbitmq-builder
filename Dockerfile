FROM ubuntu:16.04
RUN apt-get update && apt-get upgrade -y
RUN apt-get install git -y
WORKDIR /libs
RUN git clone https://github.com/alanxz/rabbitmq-c.git
WORKDIR /libs/rabbitmq-c
RUN git checkout v0.9.0
RUN apt-get install -y cmake libssl-dev
RUN mkdir build && cd build && cmake .. -DBUILD_STATIC_LIBS=ON -DBUILD_TOOLS=OFF && make && make install
RUN mv /usr/local/lib/x86_64-linux-gnu/* /usr/local/lib/ && rm -r /usr/local/lib/x86_64-linux-gnu/
RUN ldconfig
ENTRYPOINT [ "/bin/bash" ]

