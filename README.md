# Compiled libprotobuf

This image downloads and compiles protobuf library.

header files are in `/usr/local/include`
library files are in `/usr/local/lib`

## Header files
```
/usr/local/include# tree -UhCv --noreport
.
|-- [ 93K]  amqp.h
|-- [ 47K]  amqp_framing.h
|-- [7.7K]  amqp_ssl_socket.h
`-- [2.2K]  amqp_tcp_socket.h
```

## Library files

```
/usr/local/lib# tree -UhCv --noreport
.
|-- [167K]  librabbitmq.a
|-- [  16]  librabbitmq.so -> librabbitmq.so.4
|-- [  20]  librabbitmq.so.4 -> librabbitmq.so.4.3.0
|-- [115K]  librabbitmq.so.4.3.0
```
